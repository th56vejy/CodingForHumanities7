#### Check out https://cran.r-project.org/web/packages/httr/vignettes/quickstart.html
#### And https://cran.r-project.org/web/packages/jsonlite/vignettes/json-aaquickstart.html

library(httr)
library(jsonlite)
library(stringr)

my_api_key <- ""
url <- paste("http://api.digitalnz.org/v3/records.json?api_key=", my_api_key, sep = "") 
query <- "&text=earthquake&and[collection][]=New+Zealand+Cartoon+Archive"
fields1 <- "&fields=id"
page1 <- "&per_page=1&page=1"
fields2 <- "&fields=id,origin_url,title,description,collection,dc_identifier,large_thumbnail_url,authorities"
all_pages <- "&per_page=100&page="

query1 <- paste(url, query, fields1, page1, sep = "")
query2 <- paste(url, query, fields2, all_pages, sep = "")

### How many API calls

number_calls <- fromJSON(query1)$search$result_count

api_calls <- paste(query2, as.character(c(1:number_calls)), sep = "")

DataframeFromCall <- function(x) {
  data <- fromJSON(x)
  results <- data[[1]][2]
  authorities <- unlist(results, recursive = FALSE)[[1]]
  ID <- unlist(results, recursive = FALSE)[[2]]
  Origin <- unlist(results, recursive = FALSE)[[3]]
  Title <- unlist(results, recursive = FALSE)[[4]]
  Description <- unlist(results, recursive = FALSE)[[5]]
  Collection <- unlist(results, recursive = FALSE)[[6]]
  Identifier <- unlist(results, recursive = FALSE)[[7]]
  Thumbnails <- unlist(results, recursive = FALSE)[[8]]
  DCDL_IDs <- vector()
  for(i in 1:length(Identifier)) {
    DCDL_IDs[i] <- Identifier[[i]][5]
  }
  Date <- str_extract(Title, "\\d{1,2}\\s\\w*\\s\\d{4}")
  TestData <- data.frame(ID,Origin,Title,Description,Thumbnails,DCDL_IDs,Date)
  return(TestData)
} 

all.data <- data.frame(ID=character(),
                       Origin=character(), 
                       Title=character(),
                       Description=character(),
                       Thumbnails=character(),
                       DCDL_IDs=character(),
                       Date=character(),
                       stringsAsFactors=FALSE)

for (i in 1:number_calls) {
  all.data <- rbind(all.data, DataframeFromCall(api_calls[i]))
}


##################
######### TM
#################

library(lda)
library(LDAvis)

### Text cleaning function
preprocess_corpus <- function(x) {
  research_corpus <- tolower(x)  # force to lowercase
  research_corpus <- gsub("'", " ", research_corpus)  # remove apostrophes
  research_corpus <- gsub("-", "", research_corpus)  # remove hyphens
  research_corpus <- gsub("[[:punct:]]", " ", research_corpus)  # replace punctuation with space
  research_corpus <- gsub("[[:cntrl:]]", " ", research_corpus)  # replace control characters with space
  research_corpus <- trimws(research_corpus)
  research_corpus <-str_replace_all(research_corpus, "[\r\n]" , "")
  research_corpus <- gsub("^ *|(?<= ) | *$", "", research_corpus, perl = TRUE) # Remove multiple whitespace
  research_corpus <- gsub("[0-9]", "", research_corpus) #remove numbers
  research_corpus <- trimws(research_corpus)
  return(research_corpus)
}

research_corpus <- all.data$Description
is.factor(research_corpus)
research_corpus <- as.character(research_corpus)
research_corpus <- preprocess_corpus(research_corpus)
identifier <- as.character(all.data$ID)

#### Preparation

doc.list <- strsplit(research_corpus, "[[:space:]]+")
all_words <- unlist(doc.list)
# compute the table of terms:
term.table <- table(all_words)
term.table <- sort(term.table, decreasing = TRUE)

stop_words <- names(head(term.table, n = 75))

occurences <- 3

del <- names(term.table) %in% stop_words | term.table < occurences
term.table <- term.table[!del]
vocab <- names(term.table)

# now put the documents into the format required by the lda package:

get.terms <- function(x) {
  index <- match(x, vocab)
  index <- index[!is.na(index)]
  rbind(as.integer(index - 1), as.integer(rep(1, length(index))))
}

documents <- lapply(doc.list, get.terms)

# Compute some statistics related to the data set:

D <- length(documents)  # number of documents
W <- length(vocab)  # number of terms in the vocab 
doc.length <- sapply(documents, function(x) sum(x[2, ]))  # number of tokens per document
N <- sum(doc.length)  # total number of tokens in the data 
term.frequency <- as.integer(term.table)  # frequencies of terms in the corpus 

##### The actual TM

seed <- 73
set.seed(seed)
K <- 15
iterations <- 500
alpha <- 0.02
eta <- 0.1
number_terms <- 25
fit <- lda.collapsed.gibbs.sampler(documents = documents, K = K, vocab = vocab, 
                                   num.iterations = iterations, alpha = alpha, 
                                   eta = eta, initial = NULL, burnin = 0,
                                   compute.log.likelihood = TRUE)

##### The visualisation

theta <- t(apply(fit$document_sums + alpha, 2, function(x) x/sum(x)))
phi <- t(apply(t(fit$topics) + eta, 2, function(x) x/sum(x)))

research_corpusAbstracts <- list(phi = phi,
                                 theta = theta,
                                 doc.length = doc.length,
                                 vocab = vocab,
                                 term.frequency = term.frequency)

json <- createJSON(phi = research_corpusAbstracts$phi, 
                   theta = research_corpusAbstracts$theta, 
                   doc.length = research_corpusAbstracts$doc.length, 
                   vocab = research_corpusAbstracts$vocab, 
                   term.frequency = research_corpusAbstracts$term.frequency,
                   R=number_terms)

dir.create("Temp_vis")
serVis(json, out.dir = "./Temp_vis", open.browser = TRUE)


